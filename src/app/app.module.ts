import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { MaterialComponentsModule } from './components/material-components/material-components.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { MainTabsComponent } from './components/main-tabs/main-tabs.component';
import { CaseManagerComponent } from './components/case-manager/case-manager.component';
import { FilesListComponent } from './components/files-list/files-list.component';
import { FilesManagerComponent } from './components/files-manager/files-manager.component';
import { NewCaseDialogComponent } from './components/dialogs/new-case-dialog/new-case-dialog.component';
import { UploadFileDialogComponent } from './components/dialogs/upload-file-dialog/upload-file-dialog.component';
import { PdfComponent } from './components/pdf/pdf.component';

import { PdfViewerComponent } from 'ng2-pdf-viewer';

import { CasesService } from './services/cases.service';
import { FilesService } from './services/files.service';
import { StatisticsService } from './services/statistics.service';
import { CommonService } from './services/common/common.service';

import { OpenAllCasesComponent } from './components/dialogs/open-all-cases/open-all-cases.component';
import { EditMetadataComponent } from './components/file-tabs/edit-metadata/edit-metadata.component';
import { ImagesComponent } from './components/images/images.component';
import { TxtComponent } from './components/txt/txt.component';
import { ChooseCategoryComponent } from './components/choose-category/choose-category.component';
import { ChangeCategoryComponent } from './components/dialogs/change-category/change-category.component';
import { AlertComponent } from './components/dialogs/alert/alert.component';
import { FilesListService } from './services/common/files-list.service';
import { FilterByFieldPipe } from './pipes/filter-by-field.pipe';
import { ChooseSubCategoryComponent } from './components/choose-sub-category/choose-sub-category.component';
import { ChangeSubCategoryComponent } from './components/dialogs/change-sub-category/change-sub-category.component';
import { ChangeAuthorComponent } from './components/dialogs/change-author/change-author.component';
import { ChangeLanguageComponent } from './components/dialogs/change-language/change-language.component';
import { FileTabsComponent } from './components/file-tabs/file-tabs.component';
import { FileStatisticComponent } from './components/file-tabs/file-statistic/file-statistic.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MainTabsComponent,
    CaseManagerComponent,
    FilesListComponent,
    FilesManagerComponent,
    NewCaseDialogComponent,
    OpenAllCasesComponent,
    UploadFileDialogComponent,
    EditMetadataComponent,
    PdfComponent,
    PdfViewerComponent,
    ImagesComponent,
    TxtComponent,
    ChooseCategoryComponent,
    ChangeCategoryComponent,
    AlertComponent,
    FilterByFieldPipe,
    ChooseSubCategoryComponent,
    ChangeSubCategoryComponent,
    ChangeAuthorComponent,
    ChangeLanguageComponent,
    FileTabsComponent,
    FileStatisticComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialComponentsModule,
    HttpClientModule,
    FormsModule,
    FileUploadModule
  ],
  entryComponents: [NewCaseDialogComponent,
    OpenAllCasesComponent,
    UploadFileDialogComponent,
    AlertComponent,
    ChangeCategoryComponent,
    ChangeSubCategoryComponent,
    ChangeAuthorComponent,
    ChangeLanguageComponent
  ],
  providers: [CasesService, FilesService, CommonService, FilesListService, StatisticsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
