import {Injectable, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';

import { environment} from '../../environments/environment';

import {Files} from '../interfaces/files';
import {Observable} from 'rxjs/Observable';
import {RequestOptions} from '@angular/http';
import {CommonService} from './common/common.service';

@Injectable()
export class FilesService implements OnInit {
  url = environment.filesApi;
  ngOnInit(): void {
  }
  constructor(private http: HttpClient, private commonService: CommonService) { }
  removeFiles(files: string[]): Observable<any> {
    let currCaseId = this.commonService.getCurrValue()._id;
    let header = new HttpHeaders().set('caseid', currCaseId);
    return this.http.post(this.url + '/delete', files, {headers: header});
  }
  updateFile(file: Files): Observable<any> {
      return this.http.put(this.url + '/' + file.filename, file.metaData);
  }
  updateCategoryField(files: string[], category) {
    return this.http.put(this.url + '/files/Category', {files: files, category: category});
  }
  updateSubCategoryField(files: string[], subCategory) {
    return this.http.put(this.url + '/files/subCategory', {files: files, subCategory: subCategory});
  }
  updateLanguageField(files: string[], language) {
    return this.http.put(this.url + '/files/language', {files: files, language: language});
  }
  updateAuthorField(files: string[], author) {
    return this.http.put(this.url + '/files/author', {files: files, author: author});
  }
}
