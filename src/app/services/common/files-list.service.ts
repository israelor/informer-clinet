import {Injectable, OnInit} from '@angular/core';
import {Case} from '../../interfaces/case';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Files} from '../../interfaces/files';
import {CommonService} from './common.service';

@Injectable()
export class FilesListService {
  private openFilesListSource: BehaviorSubject<Files[]> = new BehaviorSubject<Files[]>(undefined);
  private selectedFilesListSource: BehaviorSubject<Files[]> = new BehaviorSubject<Files[]>(undefined);
  currentOpenFiles = this.openFilesListSource.asObservable();
  currentSelectedFiles = this.selectedFilesListSource.asObservable();
  currentCase: Case;
  constructor(private commonService: CommonService) {
    this.selectedFilesListSource.next([]);
    this.openFilesListSource.next([]);
    this.commonService.currentCase.subscribe(item => {
      if (item && this.currentCase && this.currentCase._id !== item._id) {
        this.selectedFilesListSource.next([]);
        this.openFilesListSource.next([]);
      }
      this.currentCase = item;
    });
  }
  getCurrentOpenFiles() {
    return this.openFilesListSource.getValue();
  }
  getCurrentSelectedFiles() {
    return this.selectedFilesListSource.getValue();
  }
  addFilesToSelectedFiles(file: Files, state: Boolean): void {
    let temp = this.selectedFilesListSource.getValue();
    let index = temp.indexOf(file);
    if (state) {
      if (index === -1) {
        temp.push(file);
      }
    } else {
      if (index !== -1) {
        temp.splice(index, 1);
      }
    }
    this.selectedFilesListSource.next(temp);
  }
  addFileToOpenList(file: Files) {
    let temp = this.openFilesListSource.getValue();
    const index = this.isObjectInArray(temp, file.filename);
    if (index === -1) {
      temp.push(file);
      // temp.length !== 0 ? this.tabIndex = this.openFilesList.length + 1 : this.tabIndex = 0;
      this.openFilesListSource.next(temp);
    }
  }
  removeListFromOpenFiles(ids: string[]) {
    let temp = this.openFilesListSource.getValue();
    let elemToDel = [];
    for (let i = 0; i < temp.length; i++) {
      if (ids.indexOf(temp[i].filename) !== -1) {
        elemToDel.push(i);
      }
    }
    for (let i = elemToDel.length - 1; i >= 0; i--) {
      temp.splice(elemToDel[i], 1);
    }
    this.openFilesListSource.next(temp);
  }
  removeListFromSelectedFiles(ids: string[]) {
    let temp = this.selectedFilesListSource.getValue();
    let elemToDel = [];
    for (let i = 0; i < temp.length; i++) {
      if (ids.indexOf(temp[i].filename) !== -1) {
        elemToDel.push(i);
      }
    }
    for (let i = elemToDel.length - 1; i >= 0; i--) {
      temp.splice(elemToDel[i], 1);
    }
    this.selectedFilesListSource.next(temp);
  }
  removeFileFromOpenList(file: Files) {
    let temp = this.openFilesListSource.getValue();
    const index = this.isObjectInArray(temp, file.filename);
    if (index !== -1) {
      temp.splice(index, 1);
      // this.openFilesList.length !== 0 ? this.tabIndex = this.openFilesList.length + 1 : this.tabIndex = 0;
      this.openFilesListSource.next(temp);
    }
  }
  openSelectedFiles() {
    let temp = this.selectedFilesListSource.getValue();
    for (let i = 0; i < temp.length; i++) {
      this.addFileToOpenList(temp[i]);
    }
  }

  isObjectInArray(array: Files[], value) {
    for (let i = 0; i < array.length; i++) {
      if (array[i].filename === value) {
        return i;
      }
    }
    return -1;
  }
}
