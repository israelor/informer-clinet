import {Injectable, OnInit} from '@angular/core';
import {Case} from '../../interfaces/case';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {CasesService} from '../cases.service';

@Injectable()
export class CommonService implements OnInit {
  private caseSource: BehaviorSubject<any> = new BehaviorSubject<Case>(undefined);
  private localStorageCurrCase = 'currentCase';
  currentCase = this.caseSource.asObservable();
  setCurrentCase(newCase: Case) {
    this.caseSource.next(newCase);
    if (newCase) {
      localStorage.setItem(this.localStorageCurrCase, newCase._id);
    } else {
      localStorage.removeItem(this.localStorageCurrCase);
    }
  }
  getCurrValue() {
    return this.caseSource.getValue();
  }
  constructor(public caseService: CasesService) {
    const temp = localStorage.getItem(this.localStorageCurrCase);
    if (temp) {
      caseService.getCase(temp).subscribe(data => {
        this.setCurrentCase(data);
      });
    }
  }
  refreshCase() {
    this.caseService.getCase(this.caseSource.getValue()._id).subscribe(data => {
      this.setCurrentCase(data);
    });
  }
  ngOnInit(): void {
  }
}
