import {Injectable, OnInit} from '@angular/core';
import { HttpClient} from '@angular/common/http';

import { Case } from '../interfaces/case';

import { environment } from '../../environments/environment';
import {Observable} from "rxjs/Observable";

@Injectable()
export class CasesService implements OnInit{
  constructor(private http: HttpClient) { }
  url = environment.filesApi;
  allCases: Case[];
  ngOnInit(): void {
  }
  getAllCases(): Observable<Case[]> {
    return this.http.get<Case[]>(this.url + '/cases');
  }
  getCase(caseId: string): Observable<Case> {
    return this.http.get(this.url + '/cases/' + caseId);
  }
  addCase(caseName, caseType): Observable<Case> {
    return this.http.post(this.url + '/cases', {name: caseName, type: caseType,  files: []});
  }
  deleteCase(caseId) {
    return this.http.delete(this.url + '/cases/' + caseId);
  }
}
