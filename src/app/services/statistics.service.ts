import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { FileStatistics } from '../interfaces/file-statistics';

import { environment } from '../../environments/environment';
import { Observable } from "rxjs/Observable";

@Injectable()
export class StatisticsService {

  url = environment.statisticsApi;
  constructor(private http: HttpClient) { }

  getStatistics(filename: string): Observable<FileStatistics> {
    return this.http.get(this.url + filename);
  }
}
