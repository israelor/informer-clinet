import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByField',
  pure: false
})
export class FilterByFieldPipe implements PipeTransform {

  transform(items: any[], values: string[], field: string): any[] {
    if (!items) {
      return [];
    }
    if (!values || values.length === 0) {
      return items;
    }

    return items.filter(singleItem => (values.indexOf(singleItem.metaData[field]) !== -1));
  }

}
