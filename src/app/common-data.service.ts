import {Injectable, OnInit} from '@angular/core';
import {Case} from './interfaces/case';

@Injectable()
export class CommonDataService implements OnInit {
  currentCase: Case[];
  ngOnInit(): void {
  }

  getCurrentCase(): Case[] {
    return this.currentCase;
  }
  setCurrentCase(newCase: Case[]): void {
    this.currentCase = newCase;
  }
  constructor() { }

}
