import { Component, OnInit } from '@angular/core';
import {MatDialog, MatSnackBar} from '@angular/material';
import { NewCaseDialogComponent } from '../dialogs/new-case-dialog/new-case-dialog.component';
import { OpenAllCasesComponent} from '../dialogs/open-all-cases/open-all-cases.component';
import {CasesService} from '../../services/cases.service';
import {CommonService} from '../../services/common/common.service';
import {Case} from '../../interfaces/case';
import {AlertComponent} from "../dialogs/alert/alert.component";
import {isUndefined} from "util";
import {noUndefined} from "@angular/compiler/src/util";

@Component({
  selector: 'app-case-manager',
  templateUrl: './case-manager.component.html',
  styleUrls: ['./case-manager.component.css']
})
export class CaseManagerComponent implements OnInit {

  constructor(public dialog: MatDialog,
              public caseService: CasesService,
              public snackBar: MatSnackBar,
              public commonService: CommonService) { }

  newCaseDialog(): void {
    const dialogRef = this.dialog.open(NewCaseDialogComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(caseId => {
      if (caseId) {
        this.caseService.getCase(caseId).subscribe(data => {
          this.commonService.setCurrentCase(data);
        });
      }
    });
  }
  checkIfThereIsCaseOpen(): boolean {
    return !(this.commonService.getCurrValue() !== undefined);
  }
  showCases(): void {
    const dialogRef = this.dialog.open(OpenAllCasesComponent, {width: '350px'});

    dialogRef.afterClosed().subscribe(caseId => {
      if (caseId) {
        this.caseService.getCase(caseId).subscribe(data => {
          this.commonService.setCurrentCase(data);
        });
      }
    });
  }
  deleteCase() {
    const dialogRef = this.dialog.open(AlertComponent, {data: {title: 'Are you sure you wany to delete this case?'}});
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.caseService.deleteCase(this.commonService.getCurrValue()._id).subscribe(data => {
          this.commonService.setCurrentCase(undefined);
          this.snackBar.open('Case Was Deleted Successfully', '', {duration: 1000});
        });
      }
    });
  }

  ngOnInit() {
  }

}
