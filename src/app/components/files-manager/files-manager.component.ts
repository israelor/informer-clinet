import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material';
import {UploadFileDialogComponent} from '../dialogs/upload-file-dialog/upload-file-dialog.component';
import {CommonService} from '../../services/common/common.service';
import {Files} from '../../interfaces/files';
import {FilesService} from '../../services/files.service';
import {ChangeCategoryComponent} from '../dialogs/change-category/change-category.component';
import {ChangeSubCategoryComponent} from '../dialogs/change-sub-category/change-sub-category.component';
import {ChangeLanguageComponent} from '../dialogs/change-language/change-language.component';
import {ChangeAuthorComponent} from '../dialogs/change-author/change-author.component';
import {AlertComponent} from '../dialogs/alert/alert.component';
import {FilesListService} from '../../services/common/files-list.service';


@Component({
  selector: 'app-files-manager',
  templateUrl: './files-manager.component.html',
  styleUrls: ['./files-manager.component.css']
})
export class FilesManagerComponent implements OnInit {
  selectedFiles: Files[];
  constructor(public dialog: MatDialog,
              public filesListService: FilesListService,
              public commonService: CommonService,
              public filesService: FilesService) {
  }

  ngOnInit() {
    this.filesListService.currentSelectedFiles.subscribe(item => {
      this.selectedFiles = item;
    });
  }

  openUploadFileDialog(): void {
    const dialogRef = this.dialog.open(UploadFileDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  openFiles() {
    this.filesListService.openSelectedFiles();
  }
  deleteFiles() {
    const dialogRef = this.dialog.open(AlertComponent, {data: {title: 'Are you sure you wany to delete checked files?'}});
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        let idList = this.filesListService.getCurrentSelectedFiles().map(item => {
          return item.filename;
        });
        this.filesService.removeFiles(idList).subscribe(item => {
          this.commonService.refreshCase();
          this.filesListService.removeListFromSelectedFiles(idList);
          this.filesListService.removeListFromOpenFiles(idList);
        }, item => {
          this.commonService.refreshCase();
          this.filesListService.removeListFromSelectedFiles(idList);
          this.filesListService.removeListFromOpenFiles(idList);
        });
      }
    });
  }
  changeCategory(): void {
    const dialogRef = this.dialog.open(ChangeCategoryComponent, {
      data: { selectedFiles: this.selectedFiles }
    });
  }

  changeSubCategory(): void {
    const dialogRef = this.dialog.open(ChangeSubCategoryComponent, {
      data: { selectedFiles: this.selectedFiles }
    });
  }

  changeAuthor(): void {
    const dialogRef = this.dialog.open(ChangeAuthorComponent, {
      data: { selectedFiles: this.selectedFiles }
    });
  }

  changeLanguage(): void {
    const dialogRef = this.dialog.open(ChangeLanguageComponent, {
      data: { selectedFiles: this.selectedFiles }
    });
  }

}
