import { Component, OnInit, Input } from '@angular/core';
import { Files } from '../../interfaces/files';
import { environment } from '../../../environments/environment';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-txt',
  templateUrl: './txt.component.html',
  styleUrls: ['./txt.component.css']
})
export class TxtComponent implements OnInit {
  txtSrc: string;
  @Input() fileMetaData: Files;

  constructor(public sanitizer: DomSanitizer) { 

  }
  ngOnInit() {
    this.txtSrc = environment.filesApi + "/files/" + this.fileMetaData.filename
  }

  getUrl(){
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.txtSrc)
  }

  openInNewTab(url) {
    var win = window.open(url, '_blank');
    win.focus();
  }
}
