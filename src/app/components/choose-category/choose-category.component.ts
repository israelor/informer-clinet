import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { Category } from '../../interfaces/category';
import { CommonService } from '../../services/common/common.service';
import * as categories from '../../categories.json';

@Component({
  selector: 'app-choose-category',
  templateUrl: './choose-category.component.html',
  styleUrls: ['./choose-category.component.css']
})
export class ChooseCategoryComponent implements OnInit {
  @Input() currentCategory: string;
  @Output() categoryChange = new EventEmitter<string>();
  categoryValue: string;
  currentCategoryList: Category[];

  constructor(public commonService: CommonService, ) {
    commonService.currentCase.subscribe((data) => {
      this.currentCategoryList = categories["mainCtaegories"][data.type];
    });
  }

  ngOnInit() {
    this.categoryValue = this.currentCategory;
  }

  onCategoryChange(event) {
    this.categoryChange.emit(event);
  }

  // ngOnChanges(changes: SimpleChanges) {
  //   console.log(changes.currentCategory.currentValue)
  // }

}
