import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { Category } from '../../interfaces/category';
import { CommonService } from '../../services/common/common.service';
import * as categories from '../../categories.json';

@Component({
  selector: 'app-choose-sub-category',
  templateUrl: './choose-sub-category.component.html',
  styleUrls: ['./choose-sub-category.component.css']
})
export class ChooseSubCategoryComponent implements OnInit {
  @Input() currentCategory: string;
  @Output() categoryChange = new EventEmitter<string>();
  categoryValue: string;
  currentCategoryList: Category[];

  constructor(public commonService: CommonService, ) {
    commonService.currentCase.subscribe((data) => {
      this.currentCategoryList = categories["subCategories"][data.type]
    });
  }

  ngOnInit() {
    this.categoryValue = this.currentCategory;
  }

  onCategoryChange(event) {
    this.categoryChange.emit(event);
  }

  // ngOnChanges(changes: SimpleChanges) {
  //   console.log(changes.currentCategory.currentValue)
  // }

}
