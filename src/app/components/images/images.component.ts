import { Component, OnInit, Input } from '@angular/core';
import { Files } from '../../interfaces/files';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css']
})
export class ImagesComponent implements OnInit {
  @Input() fileMetaData: Files;
  ImageSrc: string;

  ngOnInit() {
    this.ImageSrc = environment.filesApi + '/files/' + this.fileMetaData.filename;
  }
  openInNewTab(url) {
    let win = window.open(url, '_blank');
    win.focus();
  }
}
