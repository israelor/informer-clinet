import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Files } from '../../interfaces/files';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.css']
})
export class PdfComponent implements OnInit {
  @Input() fileMetaData: Files;

  pdfSrc: string;
  currentPage: string = "1";
  rotation: string = "0";
  zoom: string = "1";
  numPages: number = 1;
  loaded: number = 0;
  
  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.pdfSrc = environment.filesApi + "/files/" + this.fileMetaData.filename
  }

  openInNewTab(url) {
    var win = window.open(url, '_blank');
    win.focus();
  }

  nextPage() {
    var current = parseInt(this.currentPage);
    if (current < this.numPages)    
    this.currentPage = (current + 1).toString();
  }

  prevPage() {
    if (this.currentPage != "1")
      this.currentPage = (parseInt(this.currentPage) - 1).toString();
  }

  onLoad(pdf: PDFDocumentProxy) {
    this.numPages = pdf.numPages;
  }
  onError(error: any) {
    console.log("error")
  }
  onProgress(progressData: PDFProgressData) {
    this.loaded = (progressData.loaded * 100) / this.fileMetaData.size;
  }   
}
