import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import 'rxjs/add/observable/of';

import { FilesService } from '../../services/files.service';

import { Files } from '../../interfaces/files';
import { UploadFileDialogComponent } from '../dialogs/upload-file-dialog/upload-file-dialog.component';
import { MatDialog } from '@angular/material';
import { FilesListService } from '../../services/common/files-list.service';
import { CommonService } from '../../services/common/common.service';
import { Case } from '../../interfaces/case';
import { Category } from '../../interfaces/category';
import * as categories from '../../categories.json';

@Component({
  selector: 'app-files-list',
  templateUrl: './files-list.component.html',
  styleUrls: ['./files-list.component.css']
})
export class FilesListComponent implements OnInit, OnChanges {

  @Input() dataSource: Files[];
  currentCase: Case;
  public selectedFiles: Files[] = [];
  filter = [];
  selectedToFilter = [];
  constructor(private filesService: FilesService,
    private filesListService: FilesListService,
    private commonService: CommonService,
    private dialog: MatDialog) {
    this.commonService.currentCase.subscribe(item => {
      if (item && this.currentCase && this.currentCase._id !== item._id) {
        this.isChecked = {};
      }
      this.currentCase = item;
      this.categoryList = categories["mainCtaegories"][item.type];
    });
  }

  categoryList: Category[] = [];

  displayedColumns = [
    { 'displayName': 'Category', 'propName': 'category', 'isMetadata': true, 'az': true },
    { 'displayName': 'Sec. Category', 'propName': 'subCategory', 'isMetadata': true, 'az': true },
    { 'displayName': 'Document Name', 'propName': 'originalname', 'isMetadata': false, 'az': true },
    { 'displayName': 'Download Time', 'propName': 'downloadTime', 'isMetadata': true, 'az': true },
    { 'displayName': 'Doc. Language', 'propName': 'language', 'isMetadata': true, 'az': true },
    { 'displayName': 'Author', 'propName': 'author', 'isMetadata': true, 'az': true },
    { 'displayName': 'Reference', 'propName': 'reference', 'isMetadata': true, 'az': true }
  ];
  selectedRow: number;
  toggleAllCheck: boolean;
  isChecked: object = {};
  propName = this.displayedColumns[2];

  ngOnInit() {
  }
  ngOnChanges() {
    this.sortArray(this.dataSource, this.propName);
  }

  addFilesToSelectedFiles(file: Files, state: Boolean): void {
    this.filesListService.addFilesToSelectedFiles(file, state);
  }

  addFileToOpenFilesList(file: Files) {
    this.filesListService.addFileToOpenList(file);
  }

  openUploadFileDialog(): void {
    const dialogRef = this.dialog.open(UploadFileDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  sortArray(arr: Files[], propObj) {
    console.log(propObj)
    this.propName = propObj;
    if (propObj.az) {
      if (!propObj.isMetadata) {
        arr.sort((a, b) => a[propObj.propName] ? a[propObj.propName].localeCompare(b[propObj.propName]) : 1);
      } else {
        arr.sort((a, b) => a.metaData[propObj.propName] ? a.metaData[propObj.propName].localeCompare(b.metaData[propObj.propName]) : 1);
      }
    } else {
      if (!propObj.isMetadata) {
        arr.sort((b, a) => a[propObj.propName] ? a[propObj.propName].localeCompare(b[propObj.propName]) : 1);
      } else {
        arr.sort((b, a) => a.metaData[propObj.propName] ? a.metaData[propObj.propName].localeCompare(b.metaData[propObj.propName]) : 1);
      }
    }
    propObj.az = !propObj.az;
  }

  toggleAll(state: boolean) {
    this.isChecked = [];
    for (let i = 0; i < this.dataSource.length; i++) {
      this.addFilesToSelectedFiles(this.dataSource[i], state);
      this.isChecked[this.dataSource[i].filename] = state;
    }
  }

  selectAllCategories() {
    this.selectedToFilter = [];
    this.filter = [];
  }

  insertToFilter(value, state) {
    let index = this.selectedToFilter.indexOf(value);
    if (state) {
      if (index === -1) {
        this.selectedToFilter.push(value);
      }
    } else {
      if (index !== -1) {
        this.selectedToFilter.splice(index, 1);
      }
    }
  }

  rowOver(index) {
    this.selectedRow = index;
  }
}
