import { Component, OnInit, Input } from '@angular/core';
import { Files } from '../../interfaces/files';
import _ from 'lodash';
import {FilesService} from '../../services/files.service';
import {CommonService} from '../../services/common/common.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-file-tabs',
  templateUrl: './file-tabs.component.html',
  styleUrls: ['./file-tabs.component.css']
})
export class FileTabsComponent implements OnInit {
  
  @Input() fileMetaData: Files;
  constructor(public filesService: FilesService,
              public snackBar: MatSnackBar,
              public commonService: CommonService) { }

  ngOnInit() {
  }
}
