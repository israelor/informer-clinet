import { Component, OnInit, Input } from '@angular/core';
import { Files } from '../../../interfaces/files';
import _ from 'lodash';
import {FilesService} from '../../../services/files.service';
import {CommonService} from '../../../services/common/common.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-edit-metadata',
  templateUrl: './edit-metadata.component.html',
  styleUrls: ['./edit-metadata.component.css']
})
export class EditMetadataComponent implements OnInit {
  @Input() fileMetaData: Files;
  tempMetadata: Files;
  category: string;
  showDetails = false;
  constructor(public filesService: FilesService,
              public snackBar: MatSnackBar,
              public commonService: CommonService) { }

  ngOnInit() {
    this.tempMetadata = _.merge({}, this.fileMetaData);
    this.category = this.fileMetaData.metaData.category;
  }
  checkIfObjChange(): boolean {
    return _.isEqual(this.tempMetadata, this.fileMetaData);
  }

  editMetaData(file: Files) {
    this.filesService.updateFile(this.tempMetadata).subscribe(item => {
      this.snackBar.open(`Metadata updated successfully`, '', {duration: 2000});
      this.commonService.refreshCase();
    }, err => {
      this.snackBar.open(`error with updating the metadata`, '', {duration: 2000});
    });
  }
}
