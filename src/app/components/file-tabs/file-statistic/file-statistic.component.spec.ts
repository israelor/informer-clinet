import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileStatisticComponent } from './file-statistic.component';

describe('FileStatisticComponent', () => {
  let component: FileStatisticComponent;
  let fixture: ComponentFixture<FileStatisticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileStatisticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileStatisticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
