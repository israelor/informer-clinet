import { Component, OnInit, Input } from '@angular/core';
import { FileStatistics } from '../../../interfaces/file-statistics';

import { StatisticsService } from '../../../services/statistics.service'

import _ from 'lodash';

@Component({
  selector: 'app-file-statistic',
  templateUrl: './file-statistic.component.html',
  styleUrls: ['./file-statistic.component.css']
})

export class FileStatisticComponent implements OnInit {

  @Input() filename: string;
  objectKeys = Object.keys;
  fileStatistics: FileStatistics = {};

  showMessage: boolean;
  showLoader: boolean;

  constructor(public statisticsService:StatisticsService) { }

  ngOnInit() {
    this.showLoader = true;
    this.statisticsService.getStatistics(this.filename).subscribe(fileStatistics=> {
      this.showLoader = false;
  
      this.fileStatistics = fileStatistics;
      
      // this.fileStatistics = _.pickBy(fileStatistics, (value, key) => {
      //   return value.constructor === Array && value.length > 0;
      // });
      if(Object.keys(this.fileStatistics).length === 0) this.showMessage = true;
    }, err => {
      this.showLoader = false;
      this.showMessage = true;
    })
  }

  insertSpacesForKeys(key) {
    key = key.replace(/([a-z])([A-Z])/g, '$1 $2');
    key = key.replace(/([A-Z])([A-Z][a-z])/g, '$1 $2')
    return key.toLowerCase();
}

}
