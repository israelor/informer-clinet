import { Component } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { MatDialogRef, MatSnackBar, Sort } from '@angular/material';
import _ from 'lodash';
import { CommonService } from '../../../services/common/common.service';
import { Case } from '../../../interfaces/case';
import { environment } from '../../../../environments/environment';
import { CasesService } from '../../../services/cases.service';

@Component({
  selector: 'app-upload-file-dialog',
  templateUrl: './upload-file-dialog.component.html',
  styleUrls: ['./upload-file-dialog.component.css']
})
export class UploadFileDialogComponent {
  allowedFormats = ['PDF', 'JPEG', 'JPG', 'JFIF', 'Exif', 'TIFF', 'BMP', 'PNG'];

  url = environment.filesApi;
  private caseId: string;
  private caseFiles;
  public hasBaseDropZoneOver = false;
  public hasAnotherDropZoneOver = false;
  public categories: {} = {};
  public uploader: FileUploader = new FileUploader({
    url: this.url,
    itemAlias: 'photo'
  });
  unautoeizedFiles: string[] = [];

  public dropped(file: File) {
  }

  public validateCategories() {
    const that = this;

    const categoriesLength = Object.keys(_.pickBy(this.categories, function (categoryName, fileName) {
      const result = _.some(that.uploader.queue, function (fileItem) {
        return fileItem.file.name === fileName;
      });
      return (categoryName !== '') && result;
    })).length;

    return categoriesLength >= this.uploader.getNotUploadedItems().length;
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

  public onInputMainCategoryChange(input): void {
    this.assignMainCategoryToAll(input);
  }

  public filterFiles(): void {
    const that = this;
    this.uploader.queue = _.filter(this.uploader.queue, function (FileItem) {
      const isFileAllowed = (!_.includes(that.caseFiles, FileItem.file.name));
      const isExtentionAllowed = that.filterExtentions(FileItem.file);

      if (!isFileAllowed || isExtentionAllowed) {
        that.unautoeizedFiles.push(FileItem.file.name);
      }
      if (!isFileAllowed) {
        that.snackBar.open('some files are already exists in case', '', { duration: 4000 });
      }

      if (!isExtentionAllowed) {
        that.snackBar.open('some file formats are not allowes', '', { duration: 4000 });
      }
      return isFileAllowed && isExtentionAllowed;
    });
  }

  private filterExtentions(file): boolean {
    const fileExtention = file.name.split('.').pop().toUpperCase();
    return _.includes(this.allowedFormats, fileExtention);
  }

  private assignMainCategoryToAll(category): void {
    _.each(this.uploader.queue, (fileItem) => {
      this.categories[fileItem.file.name] = category;
    });
  }

  constructor(public commonService: CommonService,
    public dialogRef: MatDialogRef<UploadFileDialogComponent>,
    public snackBar: MatSnackBar) {
    commonService.currentCase.subscribe((data) => {
      if (data) {
        this.caseId = data._id;
        this.caseFiles = _.map(data.filesMetaData, (fileMetaData) => {
          return fileMetaData.originalname;
        });
      }
    });
    const that = this;
    this.uploader.onBuildItemForm = function (fileItem, form) {
      form.append('category', that.categories[fileItem.file.name]);
      form.append('caseId', that.caseId);
      return { fileItem, form };
    };

    this.uploader.onAfterAddingFile = (item) => {
      item.withCredentials = false;
    };

    this.uploader.onCompleteAll = function () {
      commonService.refreshCase();
      if (!that.uploader.getNotUploadedItems().length) {
        that.dialogRef.close();
      }
    };
  }
  closeModal() {
    this.dialogRef.close();
  }
}




