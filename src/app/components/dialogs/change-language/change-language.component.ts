import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FilesService} from '../../../services/files.service';
import {CommonService} from '../../../services/common/common.service';
import {Files} from '../../../interfaces/files';

@Component({
  selector: 'app-change-language',
  templateUrl: './change-language.component.html',
  styleUrls: ['./change-language.component.css']
})
export class ChangeLanguageComponent implements OnInit {
  newLanguage: string;
  constructor(public dialogRef: MatDialogRef<ChangeLanguageComponent>,
              public filesService: FilesService,
              public commonService: CommonService,
              @Inject(MAT_DIALOG_DATA) public selectedFiles: Files[]) { }

  ngOnInit() {
  }
  changeLanguage(newLanguage: string, files: Files[]) {
    let idList = files.map(item => {
      return item.filename;
    });
    this.filesService.updateLanguageField(idList, newLanguage).subscribe(item => {
      this.commonService.refreshCase();
      this.dialogRef.close();
    });
  }
  closeModal() {
    this.dialogRef.close();
  }
}
