import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FilesService} from '../../../services/files.service';
import {CommonService} from '../../../services/common/common.service';
import {Files} from '../../../interfaces/files';

@Component({
  selector: 'app-change-author',
  templateUrl: './change-author.component.html',
  styleUrls: ['./change-author.component.css']
})
export class ChangeAuthorComponent implements OnInit {
  newLanguage: string;
  constructor(public dialogRef: MatDialogRef<ChangeAuthorComponent>,
              public filesService: FilesService,
              public commonService: CommonService,
              @Inject(MAT_DIALOG_DATA) public selectedFiles: Files[]) { }

  ngOnInit() {
  }
  changeAuthor(newAuthor: string, files: Files[]) {
    let idList = files.map(item => {
      return item.filename;
    });
    this.filesService.updateAuthorField(idList, newAuthor).subscribe(item => {
      this.commonService.refreshCase();
      this.dialogRef.close();
    });
  }
  closeModal() {
    this.dialogRef.close();
  }
}
