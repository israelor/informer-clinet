import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FilesService} from '../../../services/files.service';
import {CommonService} from '../../../services/common/common.service';
import {Files} from '../../../interfaces/files';

@Component({
  selector: 'app-change-category',
  templateUrl: './change-category.component.html',
  styleUrls: ['./change-category.component.css']
})
export class ChangeCategoryComponent implements OnInit {
  newCategory: string;
  constructor(public dialogRef: MatDialogRef<ChangeCategoryComponent>,
              public filesService: FilesService,
              public commonService: CommonService,
              @Inject(MAT_DIALOG_DATA) public selectedFiles: Files[]) { }

  ngOnInit() {
  }
  changeCategory(newCategory: string, files: Files[]) {
    let idList = files.map(item => {
      return item.filename;
    });
    this.filesService.updateCategoryField(idList, newCategory).subscribe(item => {
      this.commonService.refreshCase();
      this.dialogRef.close();
    });
  }
  closeModal() {
    this.dialogRef.close();
  }
}
