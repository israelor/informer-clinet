import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

import { CasesService } from '../../../services/cases.service';
import { Case } from '../../../interfaces/case';

@Component({
  selector: 'app-new-case-dialog',
  templateUrl: './new-case-dialog.component.html',
  styleUrls: ['./new-case-dialog.component.css']
})

export class NewCaseDialogComponent implements OnInit {
  caseName: string;
  caseType = '';

  constructor(private caseService: CasesService,
    public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<NewCaseDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }
  caseTypes = [
    { displayName: "Criminal", value: "Criminal" },
    { displayName: "Civil Procedur", value: "Civil" },
  ]
  ngOnInit() {
  }

  addCase(caseName, caseType) {
    this.caseService.addCase(caseName, caseType).subscribe(data => {
      this.snackBar.open('New case added successfully', '', { duration: 1000 });
      this.dialogRef.close(data[0]._id);
    }, err => {
      this.snackBar.open(`Can't add case, Try again`, '', { duration: 1000 });
    });
  }
  closeModal() {
    this.dialogRef.close();
  }
}
