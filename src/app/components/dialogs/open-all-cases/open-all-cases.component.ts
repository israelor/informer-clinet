import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA, MatSnackBar} from '@angular/material';

import {CasesService} from '../../../services/cases.service';
import {Case} from '../../../interfaces/case';

@Component({
  selector: 'app-open-all-cases',
  templateUrl: './open-all-cases.component.html',
  styleUrls: ['./open-all-cases.component.css']
})
export class OpenAllCasesComponent implements OnInit {
  showMessage: boolean;
  overRow: number;
  allCases: Case[];
  showLoader: boolean;

  constructor(private caseService: CasesService,
              public snackBar: MatSnackBar,
              public dialogRef: MatDialogRef<OpenAllCasesComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.showLoader = true;
    this.showMessage = true;
    this.caseService.getAllCases().subscribe(data => {
      this.allCases = data;
      this.showLoader = false;
      if(this.allCases.length) this.showMessage = false;
    }, err => {
      this.showLoader = false;
    });
  }

  changeSelect(index): void {
    this.overRow = index;
  }

  closeDialog(caseID): void {
    this.dialogRef.close(caseID);
  }
}
