import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenAllCasesComponent } from './open-all-cases.component';

describe('OpenAllCasesComponent', () => {
  let component: OpenAllCasesComponent;
  let fixture: ComponentFixture<OpenAllCasesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenAllCasesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenAllCasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
