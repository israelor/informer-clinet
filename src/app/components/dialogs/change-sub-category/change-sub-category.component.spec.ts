import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeSubCategoryComponent } from './change-sub-category.component';

describe('ChangeSubCategoryComponent', () => {
  let component: ChangeSubCategoryComponent;
  let fixture: ComponentFixture<ChangeSubCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeSubCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeSubCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
