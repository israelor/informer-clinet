import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FilesService} from '../../../services/files.service';
import {CommonService} from '../../../services/common/common.service';
import {Files} from '../../../interfaces/files';

@Component({
  selector: 'app-change-sub-category',
  templateUrl: './change-sub-category.component.html',
  styleUrls: ['./change-sub-category.component.css']
})
export class ChangeSubCategoryComponent implements OnInit {
  newSubCategory: string;
  constructor(public dialogRef: MatDialogRef<ChangeSubCategoryComponent>,
              public filesService: FilesService,
              public commonService: CommonService,
              @Inject(MAT_DIALOG_DATA) public selectedFiles: Files[]) { }

  ngOnInit() {
  }
  changeSubCategory(newSubCategory: string, files: Files[]) {
    let idList = files.map(item => {
      return item.filename;
    });
    this.filesService.updateSubCategoryField(idList, newSubCategory).subscribe(item => {
      this.commonService.refreshCase();
      this.dialogRef.close();
    });
  }
  closeModal() {
    this.dialogRef.close();
  }
}
