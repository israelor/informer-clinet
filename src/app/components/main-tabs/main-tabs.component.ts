import {Component, OnChanges, OnInit} from '@angular/core';
import {CommonService} from '../../services/common/common.service';
import {Case} from '../../interfaces/case';
import {Files} from '../../interfaces/files';
import _ from 'lodash';
import {FilesListService} from '../../services/common/files-list.service';

@Component({
  selector: 'app-main-tabs',
  templateUrl: './main-tabs.component.html',
  styleUrls: ['./main-tabs.component.css']
})
export class MainTabsComponent implements OnInit {

  public currentCase: Case;
  public selectedFiles: Files[] = [];
  public openFilesList: Files[] = [];
  public tabIndex: number;
  constructor(public commonService: CommonService, public filesListService: FilesListService) { }

  ngOnInit() {
    let that = this;
    this.commonService.currentCase.subscribe(data => {
      if (data && this.currentCase && (this.currentCase._id !== data._id)) {
        this.openFilesList = [];
      } else if (data) {
        for (let i = 0; i < that.openFilesList.length; i++) {
          if (data.files.indexOf(that.openFilesList[i].filename) === 1) {
            that.openFilesList.splice(i, 1);
          }
        }
      }
      this.currentCase = data;
    });
    this.filesListService.currentOpenFiles.subscribe(data => {
      this.openFilesList = data;
      this.openFilesList.length !== 0 ? this.tabIndex = this.openFilesList.length + 1 : this.tabIndex = 0;
    });
  }
  removeFileFromOpenList(file: Files) {
    this.filesListService.removeFileFromOpenList(file);
  }

  isImage(mimeType) {
    return mimeType.startsWith('image');
  }

  IsTxtFile(file: Files) {
    const fileExtention = file.originalname.split('.').pop();
    return ((fileExtention === 'txt'));
  }

  IsDocFile(file: Files) {
    const fileExtention = file.originalname.split('.').pop();
    return ((fileExtention === 'doc') || (fileExtention === 'docx'));
  }
}
