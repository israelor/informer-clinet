import {FileMetadata} from './file-metadata';

export interface Files {
  _id: string;
  originalname: string;
  filename: string;
  path: string;
  size: number;
  metaData: FileMetadata;
}
