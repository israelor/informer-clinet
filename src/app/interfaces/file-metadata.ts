export interface FileMetadata {
  category: string;
  subCategory: string;
  language: string;
  author: string;
  reference: string;
  downloadTime: Date;
}
