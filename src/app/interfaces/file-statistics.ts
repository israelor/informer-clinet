export interface FileStatistics {
  who?: string[];
  aboutWho?: string[];
  what?: string[];
  when?: string[];
  where?: string[];
  claimExhibit?: string[];
  defenseExhibit?: string[];
  interrogationTime?: string[];
  interrogationLocation?: string[];
  exhibitFirstMentioned?: string[];
  legislativeProvisions?: string[];
  mentionedLaws?: string[];
}
