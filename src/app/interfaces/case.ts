export interface Case {
  _id?: string;
  name: string;
  files?: string[];
}
