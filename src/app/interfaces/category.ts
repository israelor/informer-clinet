export interface Category {
  hebrewName: string;
  enName: string;
}
