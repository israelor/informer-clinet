export const environment = {
  production: false,
  filesApi: 'https://informerdev.herokuapp.com',
  statisticsApi: 'https://informerdev.herokuapp.com/statistics/'
};
